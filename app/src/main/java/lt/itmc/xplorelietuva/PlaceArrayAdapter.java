package lt.itmc.xplorelietuva;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.google.android.gms.location.places.Places;

import java.util.ArrayList;
import java.util.List;

import lt.itmc.xplorelietuva.data.Place;

/**
 * Created by a.baltusis on 2016.05.10.
 */
public class PlaceArrayAdapter extends ArrayAdapter<Place> {
    public PlaceArrayAdapter(Context context, List<Place> Places) {
        super(context, 0, Places);
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Place place = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(android.R.layout.simple_list_item_1, parent, false);
        }
        TextView tvName = (TextView) convertView;
        tvName.setTextColor(Color.BLACK);
        tvName.setText(place.name);
        if (position % 2 == 0) {
            convertView.setBackgroundColor(Color.parseColor("#ffffff"));
        } else {
            convertView.setBackgroundColor(Color.parseColor("#BCF7F0"));
        }
        return convertView;
    }

}
