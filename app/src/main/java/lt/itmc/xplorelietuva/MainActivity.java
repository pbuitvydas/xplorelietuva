package lt.itmc.xplorelietuva;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.google.maps.android.geometry.Point;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import lt.itmc.xplorelietuva.data.Place;
import lt.itmc.xplorelietuva.data.PlaceGroup;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent (getApplication(), AboutActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClickCompass(View view) {
        Intent intent = new Intent(this, CompassActivity.class);
        startActivity(intent);
    }

    public void onOpenMapclicked (View view) {
        Intent intent = new Intent (this, MapsActivity.class);
        startActivity(intent);
    }

    public void onTestReadKml(View view) {
        List<PlaceGroup> groups = readKmlData();
    }

    public List<PlaceGroup> readKmlData() {
        List<PlaceGroup> groups = new ArrayList<>();
        InputStream stream = getResources().openRawResource(R.raw.lankytinos_vietos_lietuvoje);
        try {
            Document document = Jsoup.parse(stream, "UTF-8", "", Parser.xmlParser());
            Elements folders = document.getElementsByTag("Folder");
            for (Element folder : folders) {
                PlaceGroup placeGroup = new PlaceGroup();
                placeGroup.name = folder.getElementsByTag("name").iterator().next().text();
                placeGroup.places.addAll(getPlaces(folder));
                groups.add(placeGroup);
            }
        } catch (IOException e) {
            Toast.makeText(this, "Failed to read kml data", Toast.LENGTH_SHORT).show();
        }
        return groups;
    }

    public List<Place> getPlaces(Element folder) {
        List<Place> places = new ArrayList<>();
        for (Element placeMark : folder.getElementsByTag("Placemark")) {
            String text = placeMark.getElementsByTag("name").iterator().next().text();
            String coordinates = placeMark.getElementsByTag("Point").iterator().next().getElementsByTag("coordinates").iterator().next().text();
            String[] parts = coordinates.split(",");
            Place place = new Place();
            place.name = text;
            place.point = new Point(Double.valueOf(parts[0]), Double.valueOf(parts[1]));
            places.add(place);
        }
        return places;
    }
}
