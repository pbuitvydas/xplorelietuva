package lt.itmc.xplorelietuva;

import android.Manifest;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v4.content.ContextCompat;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.SphericalUtil;
import com.google.maps.android.geometry.Point;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import lt.itmc.xplorelietuva.data.Place;
import lt.itmc.xplorelietuva.data.PlaceGroup;

public class MapsActivity extends FragmentActivity implements GoogleMap.OnInfoWindowClickListener {


    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    List<PlaceGroup> groups;

    class CustomInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        private final View mContents;

        CustomInfoWindowAdapter() {
            mContents = getLayoutInflater().inflate(R.layout.custom_info_contents, null);

        }

        @Override
        public View getInfoWindow(Marker marker) {
            return null;
        }

        @Override
        public View getInfoContents(Marker marker) {
            render(marker, mContents);
            return mContents;
        }

        private void render(Marker marker, View view) {
            TextView tvName = (TextView) view.findViewById(R.id.tvName);
            TextView tvGroup = (TextView) view.findViewById(R.id.tvGroupName);
            TextView tvDistance = (TextView) view.findViewById(R.id.tvDistance);
            Place place = findPlaceByMarker(marker);
            PlaceGroup group = findPlaceGroup(place);
            Location location = mMap.getMyLocation();
            double distance = SphericalUtil.computeDistanceBetween(marker.getPosition(), new LatLng(location.getLatitude(), location.getLongitude()));
            tvName.setText(place.name);
            tvDistance.setText("Distance: " + formatNumber(distance));
            tvGroup.setText(group.name);


        }
    }

    private String formatNumber(double distance) {
        String unit = "m";
        if (distance < 1) {
            distance *= 1000;
            unit = "mm";
        } else if (distance > 1000) {
            distance /= 1000;
            unit = "km";
        }

        return String.format("%4.3f%s", distance, unit);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        setUpMapIfNeeded();
        // cia
        FloatingActionButton fabPlaces = (FloatingActionButton) findViewById(R.id.fabPlaces);
        fabPlaces.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                final List<Place> places = getAllPlaces();
                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(MapsActivity.this);
                LayoutInflater inflater = getLayoutInflater();
                View convertView = (View) inflater.inflate(R.layout.places_list, null);
                alertDialog.setView(convertView);
                alertDialog.setTitle("All places");
                final Dialog dialog = alertDialog.create();
                ListView lv = (ListView) convertView.findViewById(R.id.listView);
                final PlaceArrayAdapter adapter = new PlaceArrayAdapter(getApplicationContext(),places) ;
                lv.setAdapter(adapter);
                EditText textFilter = (EditText) convertView.findViewById(R.id.editTextFilter);
                textFilter.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        adapter.getFilter().filter(s);
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });
                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Place place = adapter.getItem(position);
                        place.marker.showInfoWindow();
                        dialog.cancel();
                    }
                });
                dialog.show();

        }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p/>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p/>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p/>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
        }
        mMap.setOnInfoWindowClickListener(this);
        mMap.setInfoWindowAdapter(new CustomInfoWindowAdapter());
        groups = readKmlData();
        for (int i = 0; i < groups.size(); i++) {
            for (int j = 0; j < groups.get(i).places.size(); j++) {
                Place place = groups.get(i).places.get(j);
                int drawableResourceId = this.getResources().getIdentifier(place.iconName, "mipmap", this.getPackageName());
                place.marker = mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(place.point.y, place.point.x))
                        .title(place.name)
                        .icon(BitmapDescriptorFactory.fromResource(drawableResourceId)));
            }

        }
    }


    public List<PlaceGroup> readKmlData() {
        List<PlaceGroup> groups = new ArrayList<>();
        InputStream stream = getResources().openRawResource(R.raw.lankytinos_vietos_lietuvoje);
        try {
            Document document = Jsoup.parse(stream, "UTF-8", "", Parser.xmlParser());
            Elements folders = document.getElementsByTag("Folder");
            for (Element folder : folders) {
                PlaceGroup placeGroup = new PlaceGroup();
                placeGroup.name = folder.getElementsByTag("name").iterator().next().text();
                placeGroup.places.addAll(getPlaces(folder));
                groups.add(placeGroup);
            }
        } catch (IOException e) {
            Toast.makeText(this, "Failed to read kml data", Toast.LENGTH_SHORT).show();
        }
        return groups;
    }

    public List<Place> getPlaces(Element folder) {
        List<Place> places = new ArrayList<>();
        for (Element placeMark : folder.getElementsByTag("Placemark")) {
            String text = placeMark.getElementsByTag("name").iterator().next().text();
            String coordinates = placeMark.getElementsByTag("Point").iterator().next().getElementsByTag("coordinates").iterator().next().text();
            String[] parts = coordinates.split(",");
            String iconName = placeMark.getElementsByTag("styleUrl").iterator().next().text();
            iconName = iconName.replace("#", "").replace("-nodesc", "").replace("-", "_");
            Place place = new Place();
            place.name = text;
            place.point = new Point(Double.valueOf(parts[0]), Double.valueOf(parts[1]));
            place.iconName = iconName;
            places.add(place);
        }
        return places;
    }

    public Place findPlaceByMarker(Marker marker) {
        for (PlaceGroup group : groups) {
            for (Place place : group.places) {
                if (place.marker.equals(marker)) {
                    return place;
                }
            }
        }
        return null;
    }

    public List<Place> getAllPlaces() {
        List<Place> places = new ArrayList<>();
        for (PlaceGroup group : groups) {
            for (Place place : group.places) {
                places.add(place);
            }
        }
        return places;
    }

    public PlaceGroup findPlaceGroup(Place placeToFind) {
        for (PlaceGroup group : groups) {
            for (Place place : group.places) {
                if (place.equals(placeToFind)) {
                    return group;
                }
            }
        }
        return null;
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
    }
}
