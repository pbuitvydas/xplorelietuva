package lt.itmc.xplorelietuva.data;

import com.google.android.gms.maps.model.Marker;
import com.google.maps.android.geometry.Point;

public class Place {
    public String name;
    public Point point;
    public Marker marker;
    public String iconName;

    @Override
    public String toString() {
        return name.toLowerCase();
    }
}
