package lt.itmc.xplorelietuva.data;

import java.util.ArrayList;
import java.util.List;

public class PlaceGroup {
    public String name;
    public List<Place> places = new ArrayList<>();
}
